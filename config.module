<?php

/**
 * Implementation of hook_init()
 */
function config_init() {
  module_load_include('php', 'config', 'config.class');
}

/**
 * Configuration get function
 * 
 * @var $preset - the name of preset
 */
function config_get($preset) {
  $hook = "config_$preset";
  $configs = array();
  foreach (module_implements($hook) as $module) {
    $configs[] = module_invoke($module, $hook);
  }
  uasort($configs, 'config_sort_weight');

  $settings = array();
  foreach ($configs as $config) {
    $settings = config_merge($settings, $config->get_settings());
  }

  return $settings;
}

/**
 * Configs list sorting by comparison callback
 */
function config_sort_weight($config1, $config2) {
  $w1 = $config1->get_weight();
  $w2 = $config2->get_weight();
  if ($w1 == $w2) {
    return 0;
  }
  return $w1 > $w2 ? 1 : -1;
}

/**
 * Just a copy of array_merge_recursive_distinct(). Renamed to avoid conflicts
 * in code, where developer can use it with original name.
 * 
 * @see http://www.php.net/manual/en/function.array-merge-recursive.php#96201
 */
function config_merge(array &$array1, array &$array2) {
  $merged = $array1;

  foreach ($array2 as $key => &$value) {
    if (is_array($value) 
        && isset($merged[$key])
        && is_array($merged[$key])) {
      $merged[$key] = config_merge($merged[$key], $value);
    }
    else {
      $merged[$key] = $value;
    }
  }

  return $merged;
}