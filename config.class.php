<?php

class Config {
  protected $weight;
  protected $settings;

  protected $weight_min = -10;
  protected $weight_max = 10;

  function __construct(array $settings = array(), $weight = 0) {
    $this->settings = $settings;
    $this->set_weight($weight);
  }

  function get_weight() {
    return $this->weight;
  }

  function set_weight($weight) {
    $this->weight = $this->check_weight($weight);
  }

  private function check_weight($weight) {
    $weight = round($weight);
    if ($weight > $this->weight_max) {
      $weight = $this->weight_max;
    }
    elseif ($weight < $this->weight_min) {
      $weight = $this->weight_min;
    }
    return $weight;
  }

  function get_settings($key = NULL) {
    $settings = $this->settings;
    if ($key && isset($settings[$key])) {
      $settings = $settings[$key];
    }
    return $settings;
  }
}